/*
    Copyright 2018 Moe Hussein / Stryker Corp

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;

namespace BL
{
    [Serializable]
    public class LicenseAdder
    {
        private bool isMultiline;
        private string startTag;
        private string endTag;
        private bool duplicateHeader;
        private List<string> fixedLicenseText;

        public LicenseAdder(string startTag, string endTag, bool isMultiline = false, bool duplicateHeader = false)
        {
            this.startTag = startTag;
            this.endTag = endTag;
            this.isMultiline = isMultiline;
            this.duplicateHeader = duplicateHeader;
        }

        public void AddLicense(List<string> licenseText)
        {
            if (this.isMultiline)
            {
                fixedLicenseText = new List<string>();
                foreach (string l in licenseText)
                {
                    fixedLicenseText.Add(@startTag + l + endTag);
                }

            }
            else
            {
                fixedLicenseText = new List<string>();
                fixedLicenseText.Add(@startTag);
                fixedLicenseText.AddRange(licenseText);
                fixedLicenseText.Add(@endTag);
            }
        }

        public bool ProcessFile(string fileName, string fileExtension, bool dryRun = true)
        {
            StreamReader file = new StreamReader(fileName);
            bool actuallyProcessed = false;

            string lineEnding = ""; // default line ending
            string fileContent = File.ReadAllText(fileName);
            if (file.Peek() >= 0)
            {
                string line1 = file.ReadLine();

                string next2char = "";
                for (int i = 0; i < 3 && fileContent.Length >= line1.Length + i + 1; i++)
                {
                    next2char = fileContent.Substring(line1.Length + i, 1);
                    if (next2char[0] == '\r' || next2char[0] == '\n')
                    {
                        lineEnding += next2char;
                    }
                    if (next2char[0] == '\n') break;
                }

            }
            file.Close();

            if (lineEnding == "")
            {
                lineEnding = Environment.NewLine;
            }
            string licenseTextWithEndings = string.Join(lineEnding, fixedLicenseText.ToArray()) + lineEnding;
            int startIndex = 0;
            if (fileExtension == ".xml")
            {
                int xmlLoc = fileContent.IndexOf("<?xml");
                if (xmlLoc >= 0)
                {
                    int newLineLoc = fileContent.IndexOf(lineEnding, xmlLoc);
                    if (newLineLoc >= xmlLoc && fileContent.Length > newLineLoc + lineEnding.Length)
                    {
                        startIndex = newLineLoc + lineEnding.Length;
                    }
                }
                if (startIndex <= 0)
                {
                    throw new ArgumentException(fileName + " is not a valid xml file (does not start with <?xml tag)!");
                }
            }
            if (fileExtension == ".sh")
            {
                int xmlLoc = fileContent.IndexOf("#!");
                if (xmlLoc >= 0)
                {
                    int newLineLoc = fileContent.IndexOf(lineEnding, xmlLoc);
                    if (newLineLoc >= xmlLoc && fileContent.Length >= newLineLoc + lineEnding.Length)
                    {
                        startIndex = newLineLoc + lineEnding.Length;
                    }
                }
            }
            if (fileContent.IndexOf(licenseTextWithEndings, startIndex) < 0)
            {
                actuallyProcessed = true;
                if (dryRun == false)
                {
                    fileContent = fileContent.Insert(startIndex, licenseTextWithEndings);
                    File.WriteAllText(fileName, fileContent);
                }

            }
            return actuallyProcessed;
        }
    }



    [Serializable]
    public class ExtensionSetting
    {
        private bool isMultiline;

        private string startTag;

        private string endTag;

        private LicenseAdder lAdder;

        public bool IsMultiline
        {
            get
            {
                return isMultiline;
            }
            set
            {
                isMultiline = value;
                LAdder = new LicenseAdder(StartTag, EndTag, isMultiline, false);
            }
        }

        public string StartTag
        {
            get
            {
                return startTag;
            }
            set
            {
                startTag = value;
                LAdder = new LicenseAdder(startTag, EndTag, IsMultiline, false);
            }
        }

        public string EndTag
        {
            get
            {
                return endTag;
            }
            set
            {
                endTag = value;
                LAdder = new LicenseAdder(StartTag, endTag, IsMultiline, false);
            }
        }

        public LicenseAdder LAdder
        {
            get
            {
                return lAdder;
            }
            set
            {
                lAdder = value;
            }
        }

        public ExtensionSetting(string startTag, string endTag, bool isMultiline = false)
        {
            StartTag = startTag;
            EndTag = endTag;
            IsMultiline = isMultiline;
            LAdder = new LicenseAdder(StartTag, EndTag, IsMultiline, false);
        }
    }
}
