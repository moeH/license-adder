﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace BL
{
    public class Helpers
    {
        private static string ResolvePaths(string path)
        {
            path = Environment.ExpandEnvironmentVariables(path);
            return Path.GetFullPath(path).TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }
        private static List<string> ResolvePaths(List<string> paths)
        {
            var FixedPaths = new List<string>();
            foreach (string path in paths)
            {
                FixedPaths.Add(ResolvePaths(path));
            }
            return FixedPaths;
        }
        public static List<string> GetFilesMissingHeader(string path, List<string> extensions, string excludePath, bool subDir = false)
        {
            List<string> filesToProcess = new List<string>();
            List<string> dirsToProcess = new List<string>();
            var excludePaths = new List<string>();
            path = ResolvePaths(path);
            try
            {
                if ((File.GetAttributes(path) & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    var whitelist = GetListPaths(path);
                    foreach (string line in whitelist)
                    {
                        if (Directory.Exists(line))
                        {
                            if ((File.GetAttributes(line) & FileAttributes.Directory) != FileAttributes.Directory)
                            {
                                if (File.Exists(line))
                                {
                                    filesToProcess.Add(line);
                                }
                            }
                            else if (Directory.Exists(line))
                            {
                                dirsToProcess.Add(line);
                            }
                        }
                    }
                }
                else
                {
                    dirsToProcess.Add(path);
                }
            }
            catch (System.Exception e)
            {
                throw new System.Exception("Wasn't able to use provided whitelist file", e);
            }

            excludePaths = GetListPaths(excludePath);

            foreach (string dir in dirsToProcess)
            {
                SearchOption option = subDir ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                foreach (string selectedItem in extensions)
                {
                    filesToProcess.AddRange(Directory.GetFiles(dir, selectedItem, option).Where(f => f.EndsWith(selectedItem.TrimStart('*'), System.StringComparison.CurrentCultureIgnoreCase)));
                }
            }

            if (excludePaths.Count > 0)
            {
                List<string> xlItems = new List<string>();
                foreach (string item in excludePaths)
                {
                    xlItems.Add(item);
                }
                filesToProcess = filesToProcess.FindAll((string s) => !xlItems.Any((string t) => s.StartsWith(t, System.StringComparison.CurrentCultureIgnoreCase)));
            }

            return filesToProcess;
        }


        public static List<string> GetListPaths(string path)
        {
            var paths = new List<string>();
            if (string.IsNullOrEmpty(path) == false)
            {
                try
                {
                    path = ResolvePaths(path);
                    if ((File.GetAttributes(path) & FileAttributes.Directory) != FileAttributes.Directory)
                    {
                        var lines = ResolvePaths(File.ReadAllLines(path).ToList<string>());
                        foreach (string line in lines)
                        {
                            if (Directory.Exists(line) || File.Exists(line))
                            {
                                if (!paths.Contains(line))
                                {
                                    paths.Add(line);
                                }
                            }
                        }
                    }
                    else if (!paths.Contains(path, StringComparer.OrdinalIgnoreCase))
                    {
                        paths.Add(path);
                    }
                }
                catch (System.Exception e)
                {
                    throw new System.Exception("Wasn't able to use provided blacklist file", e);
                }
            }
            paths.Sort();
            return paths;
        }

        public static Dictionary<string, ExtensionSetting> origSupportedExtensions = new Dictionary<string, ExtensionSetting>
        {
            {
                "*.cs",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.cpp",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.c",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.cc",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.cxx",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.h",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.hpp",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.java",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.js",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.php",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.qml",
                new ExtensionSetting("/*", "*/", false)
            },
            {
                "*.pro",
                new ExtensionSetting("# ", "", true)
            },
            {
                "*.pri",
                new ExtensionSetting("# ", "", true)
            },
            {
                "*.xml",
                new ExtensionSetting("<!--", "-->", false)
            },
            {
                "*.html",
                new ExtensionSetting("<!--", "-->", false)
            },
            {
                "*.htm",
                new ExtensionSetting("<!--", "-->", false)
            },
            {
                "*.vb",
                new ExtensionSetting("' ", "", true)
            },
            {
                "*.rb",
                new ExtensionSetting("=begin", "=end", false)
            },
            {
                "*.py",
                new ExtensionSetting("'''", "'''", false)
            },
            {
                "*.m",
                new ExtensionSetting("%{", "%}", false)
            },
            {
                "*.sh",
                new ExtensionSetting("# ", "", true)
            }
        };

    }

}
