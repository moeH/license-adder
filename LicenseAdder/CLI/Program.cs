﻿
using BL;
using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CLI
{
    class Options
    {
        [Option('h', "headerPath", Required = false,
          HelpText = "Input header file path to be processed, add a header.txt file near to this exe or provide path")]
        public string headerPath { get; set; }

        [Option('r', "rootFolder", Required = true, HelpText = "Input Target directory that contains source files or whitelist text file")]
        public string rootFolder { get; set; }

        [OptionList('t', "types", Required = true, Separator = ',',
            HelpText = "Input file types expected to have header ex: --t py cs,cpp,h or --t all for all supported types")]
        public List<string> fileTypes { get; set; }

        [Option('p', "pause", Required = false, DefaultValue = false,
         HelpText = "Pause before execution, requires key press to continue.")]
        public bool pause { get; set; }

        [Option('x', "excludePath", Required = false,
            HelpText = "Path or blacklist text file of exclusions to be exluded from this check ex: --x c:\\abcd or  --x c:\\blacklist.txt")]
        public string excludePath { get; set; }


    }
    public class Program
    {

        static void Main(string[] args)
        {
            //DefaultValue = AppDomain.CurrentDomain.BaseDirectory
            var options = new Options();

            var results = Parser.Default.ParseArgumentsStrict(args, options);

            if (string.IsNullOrEmpty(options.headerPath))
            {
                options.headerPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "header.txt");
            }
            else if (options.headerPath[1] != ':')
            {
                options.headerPath = Path.Combine(Environment.CurrentDirectory, options.headerPath);
            }

            if (options.excludePath == null)
            {
                options.excludePath = "";
            }
            if (File.Exists(options.headerPath) == false)
            {

                throw new FileNotFoundException("Couldn't find header file", options.headerPath);
            }


            var licenseText = new List<string>(File.ReadAllLines(options.headerPath));
            Dictionary<string, ExtensionSetting> supportedExtensions = new Dictionary<string, ExtensionSetting>(Helpers.origSupportedExtensions);

            foreach (ExtensionSetting value in supportedExtensions.Values)
            {
                value.LAdder.AddLicense(licenseText);
            }
            //options.rootFolder = @"C:\Users\hussem1\Desktop\Tests\Working"; // Delete this line 
            List<string> extensions = new List<string>();
            if (options.fileTypes.Contains("all") == true)
            {
                extensions = Helpers.origSupportedExtensions.Keys.ToList();
            }
            else
            {
                foreach (string extension in options.fileTypes)
                {
                    var ext = extension;
                    if (ext.StartsWith("*.") == false)
                    {
                        ext = "*." + ext;
                    }
                    if (Helpers.origSupportedExtensions.Keys.ToList().Contains(ext) == true)
                    {
                        extensions.Add(ext);
                    }
                }
            }
            if (extensions.Count == 0)
            {
                Console.WriteLine("ERROR: provided file types are missing or unsupported!");
                Environment.Exit(-1);
            }

            Console.WriteLine("Executing with the following params:");
            Console.WriteLine("Included paths:");
            Helpers.GetListPaths(options.rootFolder).ForEach(i => Console.WriteLine("\t{0}", i));
            Console.WriteLine("Extensions:");
            extensions.ForEach(i => Console.WriteLine("\t{0}", i));

            if (options.excludePath.Length > 0)
            {
                Console.WriteLine("Exclude Paths:");
                var exPaths = Helpers.GetListPaths(options.excludePath);
                exPaths.ForEach(i => Console.WriteLine("\t{0}", i));
            }
            if (options.pause == true)
            {
                Console.WriteLine("Press any key to continue or q to terminate");
                var k = Console.ReadKey();
                if (k.KeyChar == 'q')
                {
                    Environment.Exit(0);
                }
            }

            Console.Out.WriteLine("Processing... Please wait...");
            var filesToProcess = Helpers.GetFilesMissingHeader(options.rootFolder, extensions, options.excludePath, subDir: true);
            var filesProcessed = new List<string>();


            for (int i = 0; i < filesToProcess.Count; i++)
            {
                string fileName = filesToProcess[i];
                string fileExtension = Path.GetExtension(fileName).ToLower();
                bool processed = supportedExtensions["*" + fileExtension].LAdder.ProcessFile(fileName, fileExtension, dryRun: true);
                if (processed)
                {
                    filesProcessed.Add(fileName);
                    Console.WriteLine("{0} is missing header!", fileName);
                }

                //if (i % 1 == 0)
                //{
                //    worker.ReportProgress(i * 100 / filesToProcess.Count);
                //}
            }

            if (filesProcessed.Count != 0)
            {
                Console.WriteLine("ERROR: {0} files are missing header!", filesProcessed.Count);
                Environment.Exit(-10);
            }
            else
            {
                Console.Out.WriteLine("SUCCESS: checked {0} files contain expected header", filesToProcess.Count);
                Environment.Exit(0);
            }



        }
    }
}
