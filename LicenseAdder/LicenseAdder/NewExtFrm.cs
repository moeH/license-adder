using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace LicenseAdder
{
    public class NewExtFrm : Form
	{
		private Dictionary<string, ExtensionSetting> newExtObj;

		private IContainer components = null;

		private Button btnAddExt;

		private GroupBox groupBox1;

		private CheckBox chkbIsMultiline;

		private TextBox txtEndTag;

		private Label label2;

		private TextBox txtNewStartTag;

		private Label label1;

		private TextBox txtNewExt;

		private Label lblExt;

		public NewExtFrm(Dictionary<string, ExtensionSetting> newExtObj)
		{
			InitializeComponent();
			base.ActiveControl = txtNewExt;
			this.newExtObj = newExtObj;
		}

		private void btnAddExt_Click(object sender, EventArgs e)
		{
			if (!txtNewExt.Text.StartsWith("*"))
			{
				if (!txtNewExt.Text.StartsWith("."))
				{
					txtNewExt.Text = "." + txtNewExt.Text;
				}
				txtNewExt.Text = "*" + txtNewExt.Text;
			}
			newExtObj.Add(txtNewExt.Text, new ExtensionSetting(txtNewStartTag.Text, txtEndTag.Text, chkbIsMultiline.Checked));
			Close();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			btnAddExt = new Button();
			groupBox1 = new GroupBox();
			chkbIsMultiline = new CheckBox();
			txtEndTag = new TextBox();
			label2 = new Label();
			txtNewStartTag = new TextBox();
			label1 = new Label();
			txtNewExt = new TextBox();
			lblExt = new Label();
			groupBox1.SuspendLayout();
			SuspendLayout();
			btnAddExt.Location = new Point(176, 200);
			btnAddExt.Name = "btnAddExt";
			btnAddExt.Size = new Size(75, 23);
			btnAddExt.TabIndex = 5;
			btnAddExt.Text = "Add";
			btnAddExt.UseVisualStyleBackColor = true;
			btnAddExt.Click += btnAddExt_Click;
			groupBox1.Controls.Add(chkbIsMultiline);
			groupBox1.Controls.Add(txtEndTag);
			groupBox1.Controls.Add(label2);
			groupBox1.Controls.Add(txtNewStartTag);
			groupBox1.Controls.Add(label1);
			groupBox1.Controls.Add(txtNewExt);
			groupBox1.Controls.Add(lblExt);
			groupBox1.Location = new Point(12, 12);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new Size(239, 182);
			groupBox1.TabIndex = 6;
			groupBox1.TabStop = false;
			groupBox1.Text = "New File Extension";
			chkbIsMultiline.AutoSize = true;
			chkbIsMultiline.Location = new Point(84, 138);
			chkbIsMultiline.Name = "chkbIsMultiline";
			chkbIsMultiline.Size = new Size(75, 17);
			chkbIsMultiline.TabIndex = 4;
			chkbIsMultiline.Text = "Is Multiline";
			chkbIsMultiline.UseVisualStyleBackColor = true;
			txtEndTag.AcceptsTab = true;
			txtEndTag.Location = new Point(84, 105);
			txtEndTag.Name = "txtEndTag";
			txtEndTag.Size = new Size(100, 20);
			txtEndTag.TabIndex = 3;
			label2.AutoSize = true;
			label2.Location = new Point(6, 108);
			label2.Name = "label2";
			label2.Size = new Size(48, 13);
			label2.TabIndex = 12;
			label2.Text = "End Tag";
			txtNewStartTag.AcceptsTab = true;
			txtNewStartTag.Location = new Point(84, 72);
			txtNewStartTag.Name = "txtNewStartTag";
			txtNewStartTag.Size = new Size(100, 20);
			txtNewStartTag.TabIndex = 2;
			label1.AutoSize = true;
			label1.Location = new Point(6, 75);
			label1.Name = "label1";
			label1.Size = new Size(51, 13);
			label1.TabIndex = 10;
			label1.Text = "Start Tag";
			txtNewExt.AcceptsTab = true;
			txtNewExt.Location = new Point(84, 38);
			txtNewExt.Name = "txtNewExt";
			txtNewExt.Size = new Size(100, 20);
			txtNewExt.TabIndex = 1;
			lblExt.AutoSize = true;
			lblExt.Location = new Point(6, 41);
			lblExt.Name = "lblExt";
			lblExt.Size = new Size(72, 13);
			lblExt.TabIndex = 6;
			lblExt.Text = "File Extension";
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = AutoScaleMode.Font;
			BackColor = SystemColors.GradientInactiveCaption;
			base.ClientSize = new Size(260, 228);
			base.Controls.Add(groupBox1);
			base.Controls.Add(btnAddExt);
			base.Name = "NewExtFrm";
			base.StartPosition = FormStartPosition.CenterParent;
			Text = "New File Extension";
			groupBox1.ResumeLayout(false);
			groupBox1.PerformLayout();
			ResumeLayout(false);
		}
	}
}
