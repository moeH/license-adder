/*
    Copyright 2012 Peter Pratscher

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using System.Windows.Forms;

namespace LicenseAdder
{
    partial class ProcessCompletedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

      

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessCompletedForm));
            this.lblSucessfull = new System.Windows.Forms.Label();
            this.lstError = new System.Windows.Forms.ListBox();
            this.lblError = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lstSuccess = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblSucessfull
            // 
            this.lblSucessfull.AutoSize = true;
            this.lblSucessfull.Location = new System.Drawing.Point(12, 9);
            this.lblSucessfull.Name = "lblSucessfull";
            this.lblSucessfull.Size = new System.Drawing.Size(238, 13);
            this.lblSucessfull.TabIndex = 0;
            this.lblSucessfull.Text = "The following items were processed successfully:";
            // 
            // lstError
            // 
            this.lstError.FormattingEnabled = true;
            this.lstError.HorizontalScrollbar = true;
            this.lstError.Location = new System.Drawing.Point(15, 258);
            this.lstError.Name = "lstError";
            this.lstError.Size = new System.Drawing.Size(769, 212);
            this.lstError.TabIndex = 3;
            this.lstError.DoubleClick += new System.EventHandler(this.lstError_DoubleClick);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(12, 242);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(305, 13);
            this.lblError.TabIndex = 2;
            this.lblError.Text = "The following items could not be processded because of errors:";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(709, 476);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lstSuccess
            // 
            this.lstSuccess.FormattingEnabled = true;
            this.lstSuccess.HorizontalScrollbar = true;
            this.lstSuccess.Location = new System.Drawing.Point(15, 27);
            this.lstSuccess.Name = "lstSuccess";
            this.lstSuccess.Size = new System.Drawing.Size(769, 212);
            this.lstSuccess.TabIndex = 5;
            this.lstSuccess.DoubleClick += new System.EventHandler(this.lstSuccess_DoubleClick);
            // 
            // ProcessCompletedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(796, 504);
            this.Controls.Add(this.lstSuccess);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lstError);
            this.Controls.Add(this.lblSucessfull);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProcessCompletedForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Process Completed";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblSucessfull;

        public ListBox lstError;

        private Label lblError;

        private Button btnClose;

        public ListBox lstSuccess;
    }
}
