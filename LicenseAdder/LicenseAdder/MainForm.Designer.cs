/*
    Copyright 2012 Peter Pratscher

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using System.ComponentModel;
using System.Windows.Forms;

namespace LicenseAdder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.gpSettings = new System.Windows.Forms.GroupBox();
            this.btnExcludePaths = new System.Windows.Forms.Button();
            this.lblExcludePaths = new System.Windows.Forms.Label();
            this.chxIncludeSubFolders = new System.Windows.Forms.CheckBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblRootFolder = new System.Windows.Forms.Label();
            this.tbxRootFolder = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.gbLicenseText = new System.Windows.Forms.GroupBox();
            this.lblTemplates = new System.Windows.Forms.Label();
            this.cbxTemplates = new System.Windows.Forms.ComboBox();
            this.tbxLicenseText = new System.Windows.Forms.TextBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.gbFileExtensions = new System.Windows.Forms.GroupBox();
            this.btnResetExt = new System.Windows.Forms.Button();
            this.btnAddExtension = new System.Windows.Forms.Button();
            this.lbFileExtensions = new System.Windows.Forms.ListBox();
            this.folderBrowserDialogRoot = new FolderBrowserDialogEx();
            this.folderBrowserDialogExclude = new FolderBrowserDialogEx();
            this.tbxExcludePath = new System.Windows.Forms.TextBox();
            this.gpSettings.SuspendLayout();
            this.gbLicenseText.SuspendLayout();
            this.gbFileExtensions.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpSettings
            // 
            this.gpSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpSettings.Controls.Add(this.tbxExcludePath);
            this.gpSettings.Controls.Add(this.btnExcludePaths);
            this.gpSettings.Controls.Add(this.lblExcludePaths);
            this.gpSettings.Controls.Add(this.chxIncludeSubFolders);
            this.gpSettings.Controls.Add(this.btnBrowse);
            this.gpSettings.Controls.Add(this.lblRootFolder);
            this.gpSettings.Controls.Add(this.tbxRootFolder);
            this.gpSettings.Location = new System.Drawing.Point(12, 12);
            this.gpSettings.Name = "gpSettings";
            this.gpSettings.Size = new System.Drawing.Size(1205, 146);
            this.gpSettings.TabIndex = 0;
            this.gpSettings.TabStop = false;
            this.gpSettings.Text = "Settings";
            // 
            // btnExcludePaths
            // 
            this.btnExcludePaths.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcludePaths.Location = new System.Drawing.Point(1130, 69);
            this.btnExcludePaths.Name = "btnExcludePaths";
            this.btnExcludePaths.Size = new System.Drawing.Size(69, 23);
            this.btnExcludePaths.TabIndex = 13;
            this.btnExcludePaths.Text = "...";
            this.btnExcludePaths.UseVisualStyleBackColor = true;
            this.btnExcludePaths.Click += new System.EventHandler(this.btnExcludePaths_Click);
            // 
            // lblExcludePaths
            // 
            this.lblExcludePaths.AutoSize = true;
            this.lblExcludePaths.Location = new System.Drawing.Point(16, 69);
            this.lblExcludePaths.Name = "lblExcludePaths";
            this.lblExcludePaths.Size = new System.Drawing.Size(75, 13);
            this.lblExcludePaths.TabIndex = 12;
            this.lblExcludePaths.Text = "Exclude Paths";
            // 
            // chxIncludeSubFolders
            // 
            this.chxIncludeSubFolders.AutoSize = true;
            this.chxIncludeSubFolders.Cursor = System.Windows.Forms.Cursors.Default;
            this.chxIncludeSubFolders.Location = new System.Drawing.Point(97, 45);
            this.chxIncludeSubFolders.Name = "chxIncludeSubFolders";
            this.chxIncludeSubFolders.Size = new System.Drawing.Size(114, 17);
            this.chxIncludeSubFolders.TabIndex = 9;
            this.chxIncludeSubFolders.Text = "Include Subfolders";
            this.chxIncludeSubFolders.UseVisualStyleBackColor = true;
            this.chxIncludeSubFolders.CheckedChanged += new System.EventHandler(this.chxIncludeSubFolders_CheckedChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Location = new System.Drawing.Point(1130, 17);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(69, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            this.btnBrowse.MouseHover += new System.EventHandler(this.btnBrowse_MouseHover);
            // 
            // lblRootFolder
            // 
            this.lblRootFolder.AutoSize = true;
            this.lblRootFolder.Location = new System.Drawing.Point(16, 22);
            this.lblRootFolder.Name = "lblRootFolder";
            this.lblRootFolder.Size = new System.Drawing.Size(65, 13);
            this.lblRootFolder.TabIndex = 1;
            this.lblRootFolder.Text = "Root Folder:";
            // 
            // tbxRootFolder
            // 
            this.tbxRootFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxRootFolder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tbxRootFolder.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.tbxRootFolder.HideSelection = false;
            this.tbxRootFolder.Location = new System.Drawing.Point(97, 19);
            this.tbxRootFolder.Name = "tbxRootFolder";
            this.tbxRootFolder.Size = new System.Drawing.Size(1027, 20);
            this.tbxRootFolder.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(168, 51);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "Deselect All";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(168, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Select All";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gbLicenseText
            // 
            this.gbLicenseText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbLicenseText.Controls.Add(this.lblTemplates);
            this.gbLicenseText.Controls.Add(this.cbxTemplates);
            this.gbLicenseText.Controls.Add(this.tbxLicenseText);
            this.gbLicenseText.Location = new System.Drawing.Point(353, 164);
            this.gbLicenseText.Name = "gbLicenseText";
            this.gbLicenseText.Size = new System.Drawing.Size(864, 374);
            this.gbLicenseText.TabIndex = 2;
            this.gbLicenseText.TabStop = false;
            this.gbLicenseText.Text = "License Text";
            // 
            // lblTemplates
            // 
            this.lblTemplates.AutoSize = true;
            this.lblTemplates.Location = new System.Drawing.Point(6, 22);
            this.lblTemplates.Name = "lblTemplates";
            this.lblTemplates.Size = new System.Drawing.Size(59, 13);
            this.lblTemplates.TabIndex = 10;
            this.lblTemplates.Text = "Templates:";
            // 
            // cbxTemplates
            // 
            this.cbxTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxTemplates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTemplates.FormattingEnabled = true;
            this.cbxTemplates.Items.AddRange(new object[] {
            "Apache License 2.0",
            "GNU General Public License v3",
            "GNU General Public License v2",
            "BSD License",
            "MIT License",
            "Mozilla Public License v1.1"});
            this.cbxTemplates.Location = new System.Drawing.Point(71, 19);
            this.cbxTemplates.Name = "cbxTemplates";
            this.cbxTemplates.Size = new System.Drawing.Size(787, 21);
            this.cbxTemplates.TabIndex = 1;
            this.cbxTemplates.SelectedIndexChanged += new System.EventHandler(this.cbxTemplates_SelectedIndexChanged);
            // 
            // tbxLicenseText
            // 
            this.tbxLicenseText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxLicenseText.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxLicenseText.Location = new System.Drawing.Point(9, 46);
            this.tbxLicenseText.Multiline = true;
            this.tbxLicenseText.Name = "tbxLicenseText";
            this.tbxLicenseText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbxLicenseText.Size = new System.Drawing.Size(849, 322);
            this.tbxLicenseText.TabIndex = 0;
            this.tbxLicenseText.TextChanged += new System.EventHandler(this.tbxLicenseText_TextChanged);
            // 
            // btnProcess
            // 
            this.btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcess.Location = new System.Drawing.Point(1142, 544);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(75, 23);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_DoWork);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.worker_ProgressChanged);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.worker_RunWorkerCompleted);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 544);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1124, 23);
            this.progressBar.TabIndex = 4;
            // 
            // gbFileExtensions
            // 
            this.gbFileExtensions.Controls.Add(this.btnResetExt);
            this.gbFileExtensions.Controls.Add(this.btnAddExtension);
            this.gbFileExtensions.Controls.Add(this.button2);
            this.gbFileExtensions.Controls.Add(this.lbFileExtensions);
            this.gbFileExtensions.Controls.Add(this.button1);
            this.gbFileExtensions.Location = new System.Drawing.Point(12, 164);
            this.gbFileExtensions.Name = "gbFileExtensions";
            this.gbFileExtensions.Size = new System.Drawing.Size(335, 374);
            this.gbFileExtensions.TabIndex = 5;
            this.gbFileExtensions.TabStop = false;
            this.gbFileExtensions.Text = "File Extensions";
            // 
            // btnResetExt
            // 
            this.btnResetExt.Location = new System.Drawing.Point(168, 109);
            this.btnResetExt.Name = "btnResetExt";
            this.btnResetExt.Size = new System.Drawing.Size(75, 23);
            this.btnResetExt.TabIndex = 26;
            this.btnResetExt.Text = "Reset";
            this.btnResetExt.UseVisualStyleBackColor = true;
            this.btnResetExt.Click += new System.EventHandler(this.btnResetExt_Click);
            // 
            // btnAddExtension
            // 
            this.btnAddExtension.Location = new System.Drawing.Point(168, 80);
            this.btnAddExtension.Name = "btnAddExtension";
            this.btnAddExtension.Size = new System.Drawing.Size(75, 23);
            this.btnAddExtension.TabIndex = 25;
            this.btnAddExtension.Text = "Add";
            this.btnAddExtension.UseVisualStyleBackColor = true;
            this.btnAddExtension.Click += new System.EventHandler(this.btnAddExtension_Click);
            // 
            // lbFileExtensions
            // 
            this.lbFileExtensions.FormattingEnabled = true;
            this.lbFileExtensions.Items.AddRange(new object[] {
            "List of Extensions here"});
            this.lbFileExtensions.Location = new System.Drawing.Point(6, 22);
            this.lbFileExtensions.Name = "lbFileExtensions";
            this.lbFileExtensions.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbFileExtensions.Size = new System.Drawing.Size(156, 342);
            this.lbFileExtensions.TabIndex = 23;
            this.lbFileExtensions.SelectedIndexChanged += new System.EventHandler(this.lbFileExtensions_SelectedValueChanged);
            // 
            // folderBrowserDialogRoot
            // 
            this.folderBrowserDialogRoot.Description = "";
            this.folderBrowserDialogRoot.DontIncludeNetworkFoldersBelowDomainLevel = false;
            this.folderBrowserDialogRoot.NewStyle = true;
            this.folderBrowserDialogRoot.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialogRoot.SelectedPath = "";
            this.folderBrowserDialogRoot.ShowBothFilesAndFolders = true;
            this.folderBrowserDialogRoot.ShowEditBox = true;
            this.folderBrowserDialogRoot.ShowFullPathInEditBox = true;
            this.folderBrowserDialogRoot.ShowNewFolderButton = false;
            // 
            // folderBrowserDialogExclude
            // 
            this.folderBrowserDialogExclude.Description = "";
            this.folderBrowserDialogExclude.DontIncludeNetworkFoldersBelowDomainLevel = false;
            this.folderBrowserDialogExclude.NewStyle = true;
            this.folderBrowserDialogExclude.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialogExclude.SelectedPath = "";
            this.folderBrowserDialogExclude.ShowBothFilesAndFolders = true;
            this.folderBrowserDialogExclude.ShowEditBox = true;
            this.folderBrowserDialogExclude.ShowFullPathInEditBox = true;
            this.folderBrowserDialogExclude.ShowNewFolderButton = true;
            // 
            // tbxExcludePath
            // 
            this.tbxExcludePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxExcludePath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tbxExcludePath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.tbxExcludePath.HideSelection = false;
            this.tbxExcludePath.Location = new System.Drawing.Point(97, 71);
            this.tbxExcludePath.Name = "tbxExcludePath";
            this.tbxExcludePath.Size = new System.Drawing.Size(1027, 20);
            this.tbxExcludePath.TabIndex = 15;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1237, 590);
            this.Controls.Add(this.gbFileExtensions);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.gpSettings);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.gbLicenseText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(793, 585);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "License Adder";
            this.gpSettings.ResumeLayout(false);
            this.gpSettings.PerformLayout();
            this.gbLicenseText.ResumeLayout(false);
            this.gbLicenseText.PerformLayout();
            this.gbFileExtensions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox gpSettings;

        private Button btnBrowse;
        private ToolTip ttBrowse = new ToolTip();

        private Label lblRootFolder;

        private TextBox tbxRootFolder;

        private GroupBox gbLicenseText;

        private TextBox tbxLicenseText;

        private Button btnProcess;

        private CheckBox chxIncludeSubFolders;

        private BackgroundWorker worker;

        private ProgressBar progressBar;

        private Label lblTemplates;

        private ComboBox cbxTemplates;

        private Button button1;

        private Button button2;

        private GroupBox gbFileExtensions;

        private ListBox lbFileExtensions;

        private Label lblExcludePaths;

        private Button btnResetExt;

        private Button btnAddExtension;

        private Button btnExcludePaths;
        private FolderBrowserDialogEx folderBrowserDialogRoot;
        private FolderBrowserDialogEx folderBrowserDialogExclude;
        private TextBox tbxExcludePath;
    }
}

