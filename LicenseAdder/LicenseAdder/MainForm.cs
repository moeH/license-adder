/*
   Copyright 2012 Peter Pratscher

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
using BL;
using LicenseAdder.Properties;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;



namespace LicenseAdder
{
    public partial class  MainForm : Form
	{
		private List<string> filesToProcess = new List<string>();

		private List<string> filesProcessed = new List<string>();

		private List<string> filesProcessedWithError = new List<string>();

		private Dictionary<string, ExtensionSetting> supportedExtensions = new Dictionary<string, ExtensionSetting>();

		
		

		public MainForm()
		{
			InitializeComponent();
			lbFileExtensions.Items.Clear();
			chxIncludeSubFolders.Checked = Settings.Default.includeSubFolders;
			if (Settings.Default.selectedExtensions == null)
			{
				Settings.Default.selectedExtensions = new StringCollection();
				Settings.Default.Save();
			}
			if (Settings.Default.template == null)
			{
				Settings.Default.template = new StringCollection();
				Settings.Default.Save();
			}			
			List<string> tmp_selectedExtensions = new List<string>();
			StringEnumerator enumerator2 = Settings.Default.selectedExtensions.GetEnumerator();
			try
			{
				while (enumerator2.MoveNext())
				{
					string selection = enumerator2.Current;
					tmp_selectedExtensions.Add(selection);
				}
			}
			finally
			{
				(enumerator2 as IDisposable)?.Dispose();
			}
			List<string> tmp_tbxLicenseText = new List<string>();
			StringEnumerator enumerator3 = Settings.Default.template.GetEnumerator();
			try
			{
				while (enumerator3.MoveNext())
				{
					string templateLine2 = enumerator3.Current;
					tmp_tbxLicenseText.Add(templateLine2);
				}
			}
			finally
			{
				(enumerator3 as IDisposable)?.Dispose();
			}
			if (File.Exists("supportExt.bin"))
			{
				IFormatter formatter = new BinaryFormatter();
				Stream fromStream = new FileStream("supportExt.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
				supportedExtensions = (Dictionary<string, ExtensionSetting>)formatter.Deserialize(fromStream);
				fromStream.Close();
			}
			else
			{
				supportedExtensions = new Dictionary<string, ExtensionSetting>(Helpers.origSupportedExtensions);
			}
			foreach (string key in supportedExtensions.Keys)
			{
				lbFileExtensions.Items.Add(key);
				if (tmp_selectedExtensions.Contains(key))
				{
					lbFileExtensions.SetSelected(lbFileExtensions.Items.Count - 1, true);
				}
			}
			foreach (string item in tmp_tbxLicenseText)
			{
				tbxLicenseText.Lines = tmp_tbxLicenseText.ToArray();
			}
            tbxExcludePath.Text = Settings.Default.excludePath;
            folderBrowserDialogExclude.SelectedPath = Settings.Default.excludePath;

            tbxRootFolder.Text = Settings.Default.tragetPath;
			folderBrowserDialogRoot.SelectedPath = Settings.Default.tragetPath;
		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			if (Directory.Exists(tbxRootFolder.Text))
			{
				folderBrowserDialogRoot.SelectedPath = tbxRootFolder.Text;
			}
			if (folderBrowserDialogRoot.ShowDialog() == DialogResult.OK)
			{
				tbxRootFolder.Text = folderBrowserDialogRoot.SelectedPath;
				Settings.Default.tragetPath = folderBrowserDialogRoot.SelectedPath;
				Settings.Default.Save();
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(tbxLicenseText.Text))
			{
				MessageBox.Show("Please enter a license text!", "License Adder", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else if (string.IsNullOrEmpty(tbxRootFolder.Text))
			{
				MessageBox.Show("Please select the root folder to process!", "License Adder", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else if (lbFileExtensions.SelectedIndices.Count == 0)
			{
				MessageBox.Show("Please select at least one file type to process!", "License Adder", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
                try
                {
                    filesToProcess = Helpers.GetFilesMissingHeader(tbxRootFolder.Text, lbFileExtensions.SelectedItems.Cast<string>().ToList(),  tbxExcludePath.Text, chxIncludeSubFolders.Checked);
                    if (filesToProcess.Count == 0)
                    {
                        MessageBox.Show("No files were found in the selected directory!", "License Adder", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        ConfirmFilesForm frm = new ConfirmFilesForm();
                        ListBox.ObjectCollection items = frm.lstFiles.Items;
                        object[] items2 = filesToProcess.ToArray();
                        items.AddRange(items2);
                        frm.toolStripStatusLabel1.Text = string.Format("{0} files will be processed.", filesToProcess.Count.ToString());

                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            filesProcessed.Clear();
                            filesProcessedWithError.Clear();
                            btnProcess.Text = "Processing...";
                            btnProcess.Enabled = false;
                            gpSettings.Enabled = false;
                            gbLicenseText.Enabled = false;
                            progressBar.Value = 0;
                            worker.RunWorkerAsync(new List<string>(tbxLicenseText.Lines));

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "License Adder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

			}
		}

		private void worker_DoWork(object sender, DoWorkEventArgs e)
		{
			List<string> licenseText = (List<string>)e.Argument;
			foreach (ExtensionSetting value in supportedExtensions.Values)
			{
				value.LAdder.AddLicense(licenseText);
			}
			for (int i = 0; i < filesToProcess.Count; i++)
			{
				string fileName = filesToProcess[i];
				try
				{
					string fileExtension = Path.GetExtension(fileName).ToLower();
					supportedExtensions["*" + fileExtension].LAdder.ProcessFile(fileName, fileExtension, dryRun: false);
					filesProcessed.Add(fileName);
				}
				catch (Exception ex)
				{
					filesProcessedWithError.Add(ex.ToString());
				}
				if (i % 1 == 0)
				{
					worker.ReportProgress(i * 100 / filesToProcess.Count);
				}
			}
		}

		private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			progressBar.Value = e.ProgressPercentage;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < lbFileExtensions.Items.Count; i++)
			{
				lbFileExtensions.SetSelected(i, true);
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			lbFileExtensions.ClearSelected();
		}

		private void lbFileExtensions_SelectedValueChanged(object sender, EventArgs e)
		{
			Settings.Default.selectedExtensions.Clear();
			foreach (string selectedItem in lbFileExtensions.SelectedItems)
			{
				Settings.Default.selectedExtensions.Add(selectedItem);
			}
			Settings.Default.Save();
		}

		private void tbxLicenseText_TextChanged(object sender, EventArgs e)
		{
			Settings.Default.template.Clear();
			Settings.Default.template.AddRange(tbxLicenseText.Lines);
			Settings.Default.Save();
		}

		private void btnAddExtension_Click(object sender, EventArgs e)
		{
			Dictionary<string, ExtensionSetting> newExtObj = new Dictionary<string, ExtensionSetting>();
			NewExtFrm newExtFrm = new NewExtFrm(newExtObj);
			newExtFrm.ShowDialog();
			foreach (string key in newExtObj.Keys)
			{
				if (!supportedExtensions.ContainsKey(key))
				{
					lbFileExtensions.Items.Add(key);
					lbFileExtensions.SetSelected(lbFileExtensions.Items.Count - 1, true);
				}
				supportedExtensions[key] = newExtObj[key];
			}
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream("supportExt.bin", FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, supportedExtensions);
			stream.Close();
		}

		private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			progressBar.Value = 100;
			ProcessCompletedForm frm = new ProcessCompletedForm();
			if (filesProcessed.Count > 0)
			{
				ListBox.ObjectCollection items = frm.lstSuccess.Items;
				object[] items2 = filesProcessed.ToArray();
				items.AddRange(items2);
			}
			if (filesProcessedWithError.Count > 0)
			{
				ListBox.ObjectCollection items3 = frm.lstError.Items;
				object[] items2 = filesProcessedWithError.ToArray();
				items3.AddRange(items2);
			}
			frm.ShowDialog();
			btnProcess.Text = "Process";
			btnProcess.Enabled = true;
			gpSettings.Enabled = true;
			gbLicenseText.Enabled = true;
		}

		private void cbxTemplates_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbxTemplates.SelectedIndex == 0)
			{
				tbxLicenseText.Text = Resources.Apache_2_0_Template;
			}
			if (cbxTemplates.SelectedIndex == 1)
			{
				tbxLicenseText.Text = Resources.GPL_3_Template;
			}
			if (cbxTemplates.SelectedIndex == 2)
			{
				tbxLicenseText.Text = Resources.GPL_2_Template;
			}
			if (cbxTemplates.SelectedIndex == 3)
			{
				tbxLicenseText.Text = Resources.BSD_License;
			}
			if (cbxTemplates.SelectedIndex == 4)
			{
				tbxLicenseText.Text = Resources.MIT_License;
			}
			if (cbxTemplates.SelectedIndex == 5)
			{
				tbxLicenseText.Text = Resources.Mozilla_Public_License_1_1;
			}
		}

		private void btnResetExt_Click(object sender, EventArgs e)
		{
			supportedExtensions = new Dictionary<string, ExtensionSetting>(Helpers.origSupportedExtensions);
			lbFileExtensions.Items.Clear();
			foreach (string key in supportedExtensions.Keys)
			{
				lbFileExtensions.Items.Add(key);
				lbFileExtensions.SetSelected(lbFileExtensions.Items.Count - 1, true);
			}
		}

		private void btnExcludePaths_Click(object sender, EventArgs e)
		{
			if (tbxExcludePath.Text.Length > 0 && Directory.Exists(tbxRootFolder.Text))
			{
				folderBrowserDialogExclude.SelectedPath = tbxExcludePath.Text;
			}
			if (folderBrowserDialogExclude.ShowDialog() == DialogResult.OK)
			{
                tbxExcludePath.Text = folderBrowserDialogExclude.SelectedPath;
                Settings.Default.excludePath = tbxExcludePath.Text;
                Settings.Default.Save();
            }			
			
		}

		private void chxIncludeSubFolders_CheckedChanged(object sender, EventArgs e)
		{
			Settings.Default.includeSubFolders = chxIncludeSubFolders.Checked;
			Settings.Default.Save();
		}



        private void btnBrowse_MouseHover(object sender, EventArgs e)
        {
           
            ttBrowse.Show("Provide a root directory or a whitelist text file", btnBrowse);
        }

        
    }
}
