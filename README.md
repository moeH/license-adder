#### License Adder - Automatically add a license or SLA header to your source code files

### License adder is a free .NET application that allows you to easily add a license or SLA header to source files of a project. Existing headers will be updated.

# How does it work?
* Download, unzip and run the application
* Select the root folder of your project using the browse button
* Choose if files contained in sub folders should be included
* Select the file types to process
* Click Process!
# Supported Languages
* C#
* Java
* XML
* Visual Basic
* PHP
* HTML
* Java Script
* Python
* Ruby Additional languages can be process by specifying file extension, start & end tag. If your language support only single line comment, specify the same character as start & end tag.
# Notes
License adder will update any existing header in the files if it is located as the first comment before any code starts. For xml files, the license header must be located in the line after the tag

If you have any problems with the tool or an idea for a new feature, please report them in the issues section.

# Screenshots
#### Main Window:

![Main Window](docs/Main.png)


#### Confirm Selection:

![Confirm Selection](docs/Confirm.png)

#### Process Completed:

![Process Completed](docs/Completed.png)
